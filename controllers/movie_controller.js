'use strict';

var MovieModel = require('../models/movie_model'); //requiere model file.

var MovieController = function () {

}  //prototypes

/**Mapping to model**/

//Show method
MovieController.getAll = function (req, res, next) {

    MovieModel.getAll(function (err, rows) {
        if(err){
            var locals = {
                tittle : 'Error al consultar la base de datos',
                description : 'Error de sintaxis SQL.',
                error : err
            }

            // next(new Error('No hay registros de películas.'))
            res.render('error', locals);
        }else{
            var locals = {
                tittle : 'Lista de películas',
                data : rows
            }

            res.render('index', locals)
        }
    });
};

//Insert method
MovieController.insert = function (req, res, next) {

    MovieModel.lastInsertedId(function (err, row) {
        if(err){
            console.log(err)
        }else{
            var movie = {
                movie_id: parseInt(row[0].id) + 1,
                title: req.body.title,
                release_year : req.body.release_year,
                rating: req.body.rating,
                image: req.body.image
            };

            console.log(movie);

            MovieModel.insert(movie, function (err) {
                if(err){
                    var locals = {
                        tittle : 'Error al agregar el registro con el id: ' + movie.movie_id,
                        description : 'Error de sintaxis SQL.',
                        error : err
                    };

                    res.render('error', locals);
                }else{
                    res.redirect('/');
                }
            })
        }
    })
};

//Get one object
MovieController.getOne = function (req, res, next) {

    var movie_id = req.params.movie_id;

    MovieModel.getOne(movie_id, function (err, rows) {

        if(err){
            var locals = {
                tittle : 'Error al buscar el registro con el id: ' + movie_id,
                description : 'Error de sintaxis SQL.',
                error : err
            }

            res.redirect('error', locals);

        }else{
            var locals = {
                title : 'Editar Pelicula',
                data: rows
            }

            res.render('edit_movie', locals);
        }
    })
}

//Update method
MovieController.update = function (req, res, next) {

    var movie = {
        movie_id: req.params.movie_id,
        title: req.body.title,
        release_year : req.body.release_year,
        rating: req.body.rating,
        image: req.body.image
    };

    console.log(movie);

    MovieModel.update(movie, function (err) {

        if(err){
            var locals = {
                tittle : 'Error al actualizar el registro con el id: ' + movie.movie_id,
                description : 'Error de sintaxis SQL.',
                error : err
            };

            res.render('error', locals);

        }else {
            res.redirect('/');
        }
    })
}

//Delete method
MovieController.delete = function (req, res, next) {

    var movie_id = req.params.movie_id;

    MovieModel.delete(movie_id, function (err) {
        if(err){
            var locals = {
                tittle : 'Error al eliminar el registro con el id: ' + movie.movie_id,
                description : 'Error de sintaxis SQL.',
                error : err
            };

            res.render('error', locals);

        }else {
            res.redirect('/');
        }
    })
}

/**Mapping to view**/

//Add form to the views
MovieController.addForm = function (req, res, next) {
    res.render('add_movie', {title: "Agregar Pelicula"});
};

module.exports = MovieController;


