'use strict'

var mysql = require('mysql');
var conf = require('./db_conf'); //connection to database values


var dbOptions = {
    host : conf.mysql.host,
    user: conf.mysql.user,
    password: conf.mysql.pass,
    port: conf.mysql.port,
    database: conf.mysql.db
};

var myConn = mysql.createConnection(dbOptions);
myConn.connect(function (err) {
   return (err) ? console.log('Error al conectarse a mysql: ' + err.stack) : console.log('Conexxion establecida con mysql N: ' +  myConn.threadId);
});

module.exports = myConn;