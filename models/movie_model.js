'use strict';

var connect = require('./movie_connection');

var MovieModel = function (callback) {

}//prototypes

//Show function
MovieModel.getAll = function (callback) {
    connect.query('SELECT * FROM movie', callback);
};

//Insert function
MovieModel.insert = function (data, callback) {
    connect.query('INSERT INTO movie SET ? ', data, callback);
};

//Get one object
MovieModel.getOne = function (id, callback) {
    connect.query('SELECT * FROM movie WHERE movie_id = ?', id, callback);
};

//Update function
MovieModel.update = function (data, callback) {
    connect.query('UPDATE  movie SET ? WHERE movie_id = ?', [data, data.movie_id], callback);
};

//Delete function
MovieModel.delete = function (id, callback) {
    connect.query('DELETE FROM movie WHERE movie_id = ?', id, callback);
};

//Search the last movie id
MovieModel.lastInsertedId = function (callback) {
    connect.query('SELECT max(movie_id) AS id FROM movie', callback);
}

module.exports = MovieModel;
    
    
    