'use strict';

var MovieController = require('../controllers/movie_controller');
var express = require('express');
var router = express.Router();

router
    .get('/', MovieController.getAll)
    .get('/agregar', MovieController.addForm)
    .post('/', MovieController.insert)
    .get('/editar/:movie_id', MovieController.getOne)
    .put('/actualizar/:movie_id', MovieController.update) //Rest Method
    .delete('/eliminar/:movie_id', MovieController.delete); // Rest Method

module.exports = router;
